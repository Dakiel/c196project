package example.com.mobilewgu;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.Calendar;

public class AddTerm extends AppCompatActivity {

    private static final String END_DATE_CAL = "enddatecal";
    private static final int START_DIALOG_ID = 1;
    private static final int END_DIALOG_ID = 2;
    private static final String START_DATE_FIELD = "startdatefield";
    private static final String END_DATE_FIELD = "enddatefield";
    private static final String START_DATE_CAL = "startdatecal";
    private EditText addTermTitle;
    private Calendar startDate = Calendar.getInstance();
    private Calendar endDate = Calendar.getInstance();
    private Button startDateButton;
    private Button endDateButton;
    private DatabaseHelper databaseHelper;
    private DatePickerDialog.OnDateSetListener startDatePickerListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
            startDate.set(i, i1, i2);
            startDateButton.setText(formatDate(startDate));
        }
    };
    private DatePickerDialog.OnDateSetListener endDatePickerListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
            endDate.set(i, i1, i2);
            endDateButton.setText(formatDate(endDate));
        }
    };

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        startDateButton.setText(savedInstanceState.getString(START_DATE_FIELD));
        endDateButton.setText(savedInstanceState.getString(END_DATE_FIELD));
        startDate = (Calendar) savedInstanceState.getSerializable(START_DATE_CAL);
        endDate = (Calendar) savedInstanceState.getSerializable(END_DATE_CAL);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case START_DIALOG_ID:
                return new DatePickerDialog(this, startDatePickerListener, startDate.get(Calendar.YEAR), startDate.get(Calendar.MONTH), startDate.get(Calendar.DAY_OF_MONTH));
            case END_DIALOG_ID:
                return new DatePickerDialog(this, endDatePickerListener, endDate.get(Calendar.YEAR), endDate.get(Calendar.MONTH), endDate.get(Calendar.DAY_OF_MONTH));
        }
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_term);
        addTermTitle = (EditText) findViewById(R.id.addTermTitle);
        startDateButton = (Button) findViewById(R.id.selectTermStartButton);
        endDateButton = (Button) findViewById(R.id.selectTermEndButton);
        databaseHelper = DatabaseHelper.getInstance(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(START_DATE_FIELD, startDateButton.getText().toString());
        outState.putString(END_DATE_FIELD, endDateButton.getText().toString());
        outState.putSerializable(START_DATE_CAL, startDate);
        outState.putSerializable(END_DATE_CAL, endDate);
    }

    public void submitTerm(View view) {

        if (TextUtils.isEmpty(addTermTitle.getText())) {
            Toast.makeText(AddTerm.this, "All Fields must be filled!", Toast.LENGTH_SHORT).show();
            return;
        }


        if (startDate.after(endDate)) {
            Toast.makeText(AddTerm.this, "Start of term cannot be after end!", Toast.LENGTH_SHORT).show();
            return;
        }
        databaseHelper.addTerm(addTermTitle.getText().toString(), formatDate(startDate), formatDate(endDate));
        finish();


    }

    private String formatDate(Calendar calendar) {
        DateFormat dateFormat = DateFormat.getDateInstance();
        return dateFormat.format(calendar.getTime());
    }

    public void showStartPickerDialog(View view) {
        showDialog(START_DIALOG_ID);
    }

    public void showEndPickerDialog(View view) {
        showDialog(END_DIALOG_ID);
    }
}
