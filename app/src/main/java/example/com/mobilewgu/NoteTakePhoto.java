package example.com.mobilewgu;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.util.Calendar;

public class NoteTakePhoto extends Activity {

    public static final String NOTE_ID_EXTRA = "noteidextra";
    private static final String EXTRA_FILENAME = "extrafilename";
    private static final String PHOTO_TAKEN = "photo_taken";
    private DatabaseHelper databaseHelper;
    private Button takePhoto;
    private ImageView image;
    private TextView noPhoto;
    private Button saveImage;
    private boolean imgCapFlag = false;
    private boolean taken;

    private long noteId;
    private Bitmap photoBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_photos);
        image = (ImageView) findViewById(R.id.photoImage);
        noPhoto = (TextView) findViewById(R.id.noImage);
        saveImage = (Button) findViewById(R.id.saveToNote);
        databaseHelper = DatabaseHelper.getInstance(this);
        Intent getNoteIdExtraIntent = getIntent();
        noteId = getNoteIdExtraIntent.getLongExtra(NOTE_ID_EXTRA, -1);

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState.getBoolean(PHOTO_TAKEN)) {
            photoBitmap = savedInstanceState.getParcelable(EXTRA_FILENAME);
            image.setImageBitmap(photoBitmap);
            taken = true;
            imgCapFlag = true;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(PHOTO_TAKEN, taken);
        if (photoBitmap != null) {
            outState.putParcelable(EXTRA_FILENAME, photoBitmap);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        onPhotoTaken(data);


    }

    private void onPhotoTaken(Intent data) {
        taken = true;
        imgCapFlag = true;
        Bundle extras = data.getExtras();
        photoBitmap = (Bitmap) extras.get("data");
        image.setImageBitmap(photoBitmap);
        noPhoto.setVisibility(View.GONE);
    }

    public void takePhoto(View view) {
        startCameraActivity();
    }

    private void startCameraActivity() {


        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        startActivityForResult(intent, 1);
    }

    public void savePhoto(View view) {
        if (imgCapFlag) {

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            photoBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byte[] imageBlob = stream.toByteArray();
            Calendar currentDate = Calendar.getInstance();
            String imageName = formatDate(currentDate);
            databaseHelper.saveImage(imageName, imageBlob, noteId);
            finish();
        }
    }

    private String formatDate(Calendar calendar) {
        DateFormat dateFormat = DateFormat.getDateTimeInstance();
        return dateFormat.format(calendar.getTime());
    }
}