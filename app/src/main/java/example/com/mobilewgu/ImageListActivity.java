package example.com.mobilewgu;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import de.greenrobot.event.EventBus;
import example.com.mobilewgu.events.ImageListEvent;

public class ImageListActivity extends AppCompatActivity {
    public static final String NOTE_ID_EXTRA = "noteidextra";
    private DatabaseHelper databaseHelper;
    private long noteId;
    private ListView imageListView;
    private TextView noPhotos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_list);
        databaseHelper = DatabaseHelper.getInstance(this);
        imageListView = (ListView) findViewById(R.id.imageListView);
        Intent intent = getIntent();
        noteId = intent.getLongExtra(NOTE_ID_EXTRA, -1);
        noPhotos = (TextView) findViewById(R.id.noPhotos);

    }

    @SuppressWarnings("unused")
    public void onEventMainThread(ImageListEvent event) {
        loadImageList(event.getCursor());
    }

    private void loadImageList(Cursor cursor) {
        String[] from = {DatabaseHelper.IMAGES_COLUMNS[1]};
        int[] to = {android.R.id.text1};
        CursorAdapter cursorAdapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_1, cursor, from, to, 0);
        imageListView.setAdapter(cursorAdapter);
        imageListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getApplicationContext(), SingleImage.class);
                intent.putExtra(SingleImage.IMAGE_ID_EXTRA, l);
                startActivity(intent);
            }
        });
        if (imageListView.getCount() > 0) {
            noPhotos.setVisibility(View.GONE);
        } else {
            noPhotos.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.image_list_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.takePhoto:
                Intent intent = new Intent(this, NoteTakePhoto.class);
                intent.putExtra(NoteTakePhoto.NOTE_ID_EXTRA, noteId);
                startActivity(intent);
                return true;
        }
        return false;
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        databaseHelper.loadImageList(noteId);
    }
}
