package example.com.mobilewgu;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import de.greenrobot.event.EventBus;
import example.com.mobilewgu.events.SingleMentorLoadedEvent;


public class AddOrEditCourseMentor extends AppCompatActivity {

    public static final String COURSE_ID_EXTRA = "courseidextra";
    public static final String COURSE_MENTOR_ID_EXTRA = "coursementoridextra";
    private DatabaseHelper databaseHelper;
    private EditText mentorName;
    private EditText mentorEmail;
    private EditText mentorPhone;
    private TextView addMentorLabel;
    private long courseId;
    private long courseMentorId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_course_mentor_to_course);
        mentorName = (EditText) findViewById(R.id.courseMentorName);
        mentorEmail = (EditText) findViewById(R.id.courseMentorEmail);
        mentorPhone = (EditText) findViewById(R.id.courseMentorPhone);
        addMentorLabel = (TextView) findViewById(R.id.addMentorLabel);
        Intent intent = getIntent();
        courseId = intent.getLongExtra(COURSE_ID_EXTRA, -1);
        courseMentorId = intent.getLongExtra(COURSE_MENTOR_ID_EXTRA, -1);
        databaseHelper = DatabaseHelper.getInstance(this);
        if (courseMentorId != -1)
            addMentorLabel.setText("Edit Course Mentor");


    }

    @SuppressWarnings("unused")
    public void onEventMainThread(SingleMentorLoadedEvent event) {
        loadMentorData(event.getCursor());
    }

    private void loadMentorData(Cursor cursor) {
        cursor.moveToFirst();
        mentorName.setText(cursor.getString(1));
        mentorPhone.setText(cursor.getString(2));
        mentorEmail.setText(cursor.getString(3));
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        if (courseMentorId != -1) {
            getMentorData();
        }
    }

    private void getMentorData() {
        databaseHelper.loadSingleMentor(courseMentorId);
    }

    public void submitCourseMentor(View view) {
//        if mentor id is -1, we are adding a new mentor.
        if (courseMentorId == -1) {
            if (TextUtils.isEmpty(mentorName.getText()) || TextUtils.isEmpty(mentorEmail.getText()) || TextUtils.isEmpty(mentorPhone.getText())) {
                Toast.makeText(AddOrEditCourseMentor.this, "All fields must be filled!", Toast.LENGTH_SHORT).show();
                return;
            }
            databaseHelper.addCourseMentor(mentorName.getText().toString(), mentorPhone.getText().toString(), mentorEmail.getText().toString(), courseId);
            Toast.makeText(AddOrEditCourseMentor.this, "Mentor added!", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        if (TextUtils.isEmpty(mentorName.getText()) || TextUtils.isEmpty(mentorEmail.getText()) || TextUtils.isEmpty(mentorPhone.getText())) {
            Toast.makeText(AddOrEditCourseMentor.this, "All fields must be filled!", Toast.LENGTH_SHORT).show();
            return;
        }
        databaseHelper.updateSingleMentor(courseMentorId, mentorName.getText().toString(), mentorPhone.getText().toString(), mentorEmail.getText().toString());
        Toast.makeText(AddOrEditCourseMentor.this, "Mentor updated!", Toast.LENGTH_SHORT).show();
        finish();

    }
}
