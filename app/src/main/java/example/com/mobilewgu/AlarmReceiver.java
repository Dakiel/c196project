package example.com.mobilewgu;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;


/**
 * Created by beams on 5/17/2016.
 */
public class AlarmReceiver extends BroadcastReceiver {

    public static final String ALERT_MESSAGE_EXTRA = "alertmessageextra";
    public static final String ALERT_TITLE_EXTRA = "alertTitleExtra";
    public static final int REQUEST_CODE = 1337;

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent notificationIntent = new Intent(context, MainActivity.class);
        Bundle bundle = intent.getExtras();
        String message = bundle.getString(ALERT_MESSAGE_EXTRA);
        String title = bundle.getString(ALERT_TITLE_EXTRA);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(notificationIntent);

        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);

        Notification notification = builder.setContentTitle(title).setContentText(message).setTicker("Mobile WGU").setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pendingIntent).build();
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(0, notification);
    }
}
