package example.com.mobilewgu;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import de.greenrobot.event.EventBus;
import example.com.mobilewgu.events.SingleAssessmentLoadedEvent;

public class AssessmentDetailViewActivity extends AppCompatActivity {
    public static final String ASSESSMENT_ID_EXTRA = "assessmentidextra";
    private TextView title;
    private TextView dueDate;
    private Calendar dueDateCal = Calendar.getInstance();
    private DatabaseHelper databaseHelper;
    private long assessmentId;
    private TextView assessmentType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assessment_detail_view);
        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.AssessmentRelativeLayout);
        title = (TextView) findViewById(R.id.assessmentViewTitle);
        dueDate = (TextView) findViewById(R.id.assessmentViewDueDate);
        createAssessmentTypeView(relativeLayout);
        databaseHelper = DatabaseHelper.getInstance(this);
        Intent intent = getIntent();
        assessmentId = intent.getLongExtra(ASSESSMENT_ID_EXTRA, -1);
    }

    // Programmatic UI
    private void createAssessmentTypeView(RelativeLayout relativeLayout) {
        assessmentType = new TextView(this);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.CENTER_VERTICAL);
        params.addRule(RelativeLayout.ALIGN_END, R.id.assessmentViewDueDate);
        assessmentType.setTextAppearance(this, android.R.style.TextAppearance_Large);
        relativeLayout.addView(assessmentType, params);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.assessment_detail_view_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.editAssessment:
                Intent intent = new Intent(this, AddorEditAssessmentActivity.class);
                intent.putExtra(AddorEditAssessmentActivity.ASSESSMENT_ID_EXTRA, assessmentId);
                startActivity(intent);
                return true;
            case R.id.deleteAssessment:
                databaseHelper.deleteAssessment(assessmentId);
                Toast.makeText(AssessmentDetailViewActivity.this, "Assessment Deleted!", Toast.LENGTH_SHORT).show();
                finish();
                return true;
            case R.id.registerAssessmentAlert:
                registerAssessmentAlert();
                return true;
        }
        return false;
    }

    private void registerAssessmentAlert() {
        Intent intent = new Intent(this, AlarmReceiver.class);
        intent.putExtra(AlarmReceiver.ALERT_MESSAGE_EXTRA, title.getText().toString());
        intent.putExtra(AlarmReceiver.ALERT_TITLE_EXTRA, "Assesment Today!");
        PendingIntent sender = PendingIntent.getBroadcast(this, AlarmReceiver.REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, dueDateCal.getTimeInMillis(), sender);
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        databaseHelper.loadSingleAssessment(assessmentId);
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(SingleAssessmentLoadedEvent event) {
        loadAssessment(event.getCursor());
    }

    private void loadAssessment(Cursor cursor) {
        cursor.moveToFirst();
        title.setText(cursor.getString(1));
        dueDate.setText(cursor.getString(2));
        assessmentType.setText(cursor.getString(3));
        DateFormat simpleDateFormat = SimpleDateFormat.getDateInstance();
        try {
            dueDateCal.setTime(simpleDateFormat.parse(dueDate.getText().toString()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
