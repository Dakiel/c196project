package example.com.mobilewgu;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import de.greenrobot.event.EventBus;
import example.com.mobilewgu.events.AssessmentsLoadedEvent;

public class AssessmentListActivity extends AppCompatActivity {
    public static final String COURSE_ID_EXTRA = "courseidextra";
    private DatabaseHelper databaseHelper;
    private ListView assessmentListView;
    private long courseId;
    private TextView noAssessments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assessment_list);
        databaseHelper = DatabaseHelper.getInstance(this);
        Intent intent = getIntent();
        courseId = intent.getLongExtra(COURSE_ID_EXTRA, -1);
        assessmentListView = (ListView) findViewById(R.id.assessmentListView);
        noAssessments = (TextView) findViewById(R.id.noAssessments);
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(AssessmentsLoadedEvent event) {
        loadAssessments(event.getCursor());
    }

    private void loadAssessments(Cursor cursor) {
        String[] from = {DatabaseHelper.ASSESSMENT_COLUMNS[1]};
        int[] to = {android.R.id.text1};
        CursorAdapter cursorAdapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_1, cursor, from, to, 0);
        assessmentListView.setAdapter(cursorAdapter);
        assessmentListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getApplicationContext(), AssessmentDetailViewActivity.class);
                intent.putExtra(AssessmentDetailViewActivity.ASSESSMENT_ID_EXTRA, l);
                startActivity(intent);
            }
        });
        int children = assessmentListView.getCount();
        if (children > 0) {
            noAssessments.setVisibility(View.GONE);
        }
        if (children <= 0) {
            noAssessments.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.assessment_list_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.addAssessment:
                Intent intent = new Intent(this, AddorEditAssessmentActivity.class);
                intent.putExtra(AddorEditAssessmentActivity.COURSE_ID_EXTRA, courseId);
                startActivity(intent);
                return true;
        }
        return false;
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        databaseHelper.loadAssessments(courseId);

    }
}

