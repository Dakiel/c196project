package example.com.mobilewgu;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import de.greenrobot.event.EventBus;
import example.com.mobilewgu.events.CoursesLoadedEvent;
import example.com.mobilewgu.events.TermDateLoadedEvent;


public class TermActivity extends AppCompatActivity {
    public static final String EXTRA_POSITION = "position";
    private ListView courseListView;
    private DatabaseHelper databaseHelper;
    private long termId;
    private String termTitle;
    private Calendar termDueDate;
    private Calendar termStartDate;
    private TextView termTitleTextView;
    private TextView termStartDateText;
    private TextView termEndDateText;

    private TextView noCourses;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term);
        databaseHelper = DatabaseHelper.getInstance(this);
        Intent myIntent = getIntent();
        courseListView = (ListView) findViewById(R.id.courseListView);
        termId = myIntent.getLongExtra(EXTRA_POSITION, 0);
        termDueDate = Calendar.getInstance();
        termStartDate = Calendar.getInstance();
        termTitleTextView = (TextView) findViewById(R.id.termTitle);
        termStartDateText = (TextView) findViewById(R.id.termStartDate);
        termEndDateText = (TextView) findViewById(R.id.termEndDate);
        noCourses = (TextView) findViewById(R.id.noCourses);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.term_menu, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (courseListView.getChildCount() > 0) {
            menu.findItem(R.id.deleteTerm).setVisible(false);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.addCourse:
                Intent intent = new Intent(this, AddOrEditCourse.class);
                intent.putExtra(AddOrEditCourse.TERM_ID_EXTRA, termId);
                startActivity(intent);
                return true;
            case R.id.deleteTerm:
                databaseHelper.deleteTerm(termId);
                Toast.makeText(TermActivity.this, "Term Deleted", Toast.LENGTH_SHORT).show();
                finish();
                return true;
            case R.id.registerTermAlert:
                registerTermAlert();
                return true;

        }
        return false;
    }

    private void registerTermAlert() {
        Intent intent = new Intent(this, AlarmReceiver.class);
        intent.putExtra(AlarmReceiver.ALERT_MESSAGE_EXTRA, termTitle);
        intent.putExtra(AlarmReceiver.ALERT_TITLE_EXTRA, "Term Ends!");
        PendingIntent sender = PendingIntent.getBroadcast(this, AlarmReceiver.REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, termDueDate.getTimeInMillis(), sender);
        Toast.makeText(TermActivity.this, "Alert Registered!", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Override
    protected void onResume() {

        super.onResume();
        EventBus.getDefault().register(this);
        databaseHelper.loadTermCourses(termId);
        databaseHelper.loadTermData(termId);


    }

    @SuppressWarnings("unused")
    public void onEventMainThread(CoursesLoadedEvent event) {

        loadCourses(event.getCursor());

    }

    private void loadCourses(Cursor cursor) {
        String[] from = {DatabaseHelper.TERM_COLUMNS[1], "_id"};
        int[] to = {android.R.id.text1};
        CursorAdapter cursorAdapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_1, cursor, from, to, 0);


        courseListView.setAdapter(cursorAdapter);
        courseListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getApplicationContext(), CourseActivity.class);
                intent.putExtra(CourseActivity.COURSE_ID_EXTRA, l);
                startActivity(intent);
            }
        });
        if (courseListView.getCount() > 0)
            noCourses.setVisibility(View.GONE);
        else {
            noCourses.setVisibility(View.VISIBLE);
        }


    }

    @SuppressWarnings("unused")
    public void onEventMainThread(TermDateLoadedEvent event) {
        loadTermData(event.getCursor());
    }

    private void loadTermData(Cursor cursor) {

        cursor.moveToFirst();
        termTitle = cursor.getString(0);
        termTitleTextView.setText(termTitle);
        termEndDateText.setText(cursor.getString(1));
        termStartDateText.setText(cursor.getString(2));
        DateFormat simpleDateFormat1 = SimpleDateFormat.getDateInstance();
        try {
            termDueDate.setTime(simpleDateFormat1.parse(cursor.getString(1)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
