package example.com.mobilewgu;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import de.greenrobot.event.EventBus;
import example.com.mobilewgu.events.SingleMentorLoadedEvent;

public class CourseMentorDetailView extends AppCompatActivity {
    public static final String MENTOR_ID_EXTRA = "mentorid";
    private TextView name;
    private TextView email;
    private TextView phone;
    private long mentorId;
    private DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_mentor_detail_view);
        name = (TextView) findViewById(R.id.courseMentorDetailName);
        email = (TextView) findViewById(R.id.courseMentorDetailEmail);
        phone = (TextView) findViewById(R.id.courseMentorDetailPhone);
        Intent intent = getIntent();
        mentorId = intent.getLongExtra(MENTOR_ID_EXTRA, 0);
        databaseHelper = DatabaseHelper.getInstance(this);
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(SingleMentorLoadedEvent event) {
        loadMentor(event.getCursor());
    }

    private void loadMentor(Cursor cursor) {
        cursor.moveToFirst();
        name.setText(cursor.getString(1));
        phone.setText(cursor.getString(2));
        email.setText(cursor.getString(3));
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        databaseHelper.loadSingleMentor(mentorId);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.course_mentor_detail_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.deleteMentor:
                databaseHelper.deleteSingleMentor(mentorId);
                Toast.makeText(CourseMentorDetailView.this, "Mentor Deleted!", Toast.LENGTH_SHORT).show();
                finish();
                return true;
            case R.id.editMentor:
                Intent intent = new Intent(this, AddOrEditCourseMentor.class);
                intent.putExtra(AddOrEditCourseMentor.COURSE_MENTOR_ID_EXTRA, mentorId);
                startActivity(intent);
                return true;

        }
        return false;


    }
}
