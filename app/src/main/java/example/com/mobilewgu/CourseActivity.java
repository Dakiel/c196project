package example.com.mobilewgu;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import de.greenrobot.event.EventBus;
import example.com.mobilewgu.events.SingleCourseLoadedEvent;

public class CourseActivity extends AppCompatActivity {
    public static final String COURSE_ID_EXTRA = "courseid";
    private TextView courseName;
    private TextView courseStartDate;
    private TextView courseEndDate;
    private TextView courseStatus;
    private Calendar courseStartDateCal = Calendar.getInstance();
    private Calendar courseEndDateCal = Calendar.getInstance();
    private DatabaseHelper databaseHelper;
    private long courseId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        databaseHelper = DatabaseHelper.getInstance(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course);
        courseName = (TextView) findViewById(R.id.courseName);
        courseStartDate = (TextView) findViewById(R.id.courseStartDate);
        courseEndDate = (TextView) findViewById(R.id.courseEndDate);
        courseStatus = (TextView) findViewById(R.id.courseStatus);
        Intent intent = getIntent();
        courseId = intent.getLongExtra(COURSE_ID_EXTRA, -1);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.course_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.deleteCourse:
                databaseHelper.deleteCourse(courseId);
                Toast.makeText(CourseActivity.this, "Course Deleted!", Toast.LENGTH_SHORT).show();
                finish();
                return true;
            case R.id.viewCourseMentor:
                Intent intent = new Intent(this, CourseMentorList.class);
                intent.putExtra(CourseMentorList.COURSE_ID_EXTRA, courseId);
                startActivity(intent);
                return true;
            case R.id.editCourse:
                intent = new Intent(this, AddOrEditCourse.class);
                intent.putExtra(AddOrEditCourse.COURSE_ID_EXTRA, courseId);
                startActivity(intent);
                return true;
            case R.id.viewAssessments:
                intent = new Intent(this, AssessmentListActivity.class);
                intent.putExtra(AssessmentListActivity.COURSE_ID_EXTRA, courseId);
                startActivity(intent);
                return true;
            case R.id.viewNotes:
                intent = new Intent(this, NotesListActivity.class);
                intent.putExtra(NotesListActivity.COURSE_ID_EXTRA, courseId);
                startActivity(intent);
                return true;
            case R.id.registerCourseAlert:
                registerCourseAlert();
                return true;
        }
        return false;
    }


    private void registerCourseAlert() {
        Intent intent = new Intent(this, AlarmReceiver.class);
        intent.putExtra(AlarmReceiver.ALERT_MESSAGE_EXTRA, courseName.getText().toString());
        intent.putExtra(AlarmReceiver.ALERT_TITLE_EXTRA, "Course Ends!");
        PendingIntent sender = PendingIntent.getBroadcast(this, AlarmReceiver.REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, courseEndDateCal.getTimeInMillis(), sender);
        intent = new Intent(this, AlarmReceiver.class);
        intent.putExtra(AlarmReceiver.ALERT_MESSAGE_EXTRA, courseName.getText().toString());
        intent.putExtra(AlarmReceiver.ALERT_TITLE_EXTRA, "Course Started!");
        sender = PendingIntent.getBroadcast(this, AlarmReceiver.REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, courseStartDateCal.getTimeInMillis(), sender);
        Toast.makeText(CourseActivity.this, "Alert Registered!", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        databaseHelper.loadIndividualCourse(courseId);
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(SingleCourseLoadedEvent event) {
        loadCourse(event.getCursor());
    }

    private void loadCourse(Cursor cursor) {
        cursor.moveToFirst();
        courseName.setText(cursor.getString(1));
        courseStartDate.setText(cursor.getString(2));
        courseEndDate.setText(cursor.getString(3));
        courseStatus.setText(cursor.getString(4));
        DateFormat simpleDateFormat = SimpleDateFormat.getDateInstance();
        try {
            courseStartDateCal.setTime(simpleDateFormat.parse(courseStartDate.getText().toString()));
            courseEndDateCal.setTime(simpleDateFormat.parse(courseEndDate.getText().toString()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
