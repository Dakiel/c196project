package example.com.mobilewgu;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Process;

import de.greenrobot.event.EventBus;
import example.com.mobilewgu.events.AssessmentsLoadedEvent;
import example.com.mobilewgu.events.CourseMentorsLoadedEvent;
import example.com.mobilewgu.events.CoursesLoadedEvent;
import example.com.mobilewgu.events.ImageListEvent;
import example.com.mobilewgu.events.NotesLoadedEvent;
import example.com.mobilewgu.events.SingleAssessmentLoadedEvent;
import example.com.mobilewgu.events.SingleCourseLoadedEvent;
import example.com.mobilewgu.events.SingleImageLoadedEvent;
import example.com.mobilewgu.events.SingleMentorLoadedEvent;
import example.com.mobilewgu.events.SingleNoteLoadedEvent;
import example.com.mobilewgu.events.TermDateLoadedEvent;
import example.com.mobilewgu.events.TermLoadedEvent;

/**
 * Created by beams on 5/10/2016.
 */
class DatabaseHelper extends SQLiteOpenHelper {
    public static final String[] TERM_COLUMNS = {"_id", "title", "startDate", "endDate"};
    public static final String[] COURSE_COLUMNS = {"_id", "title", "startDate", "endDate", "status", "term"};
    public static final String[] COURSE_MENTOR_COLUMNS = {"_id", "name", "phone", "email", "courseId"};
    public static final String[] ASSESSMENT_COLUMNS = {"_id", "title", "dueDate", "courseId"};
    public static final String[] NOTES_COLUMNS = {"_id", "noteTitle", "noteText", "courseId"};
    public static final String[] IMAGES_COLUMNS = {"_id", "date", "image", "noteId"};
    private static final String DATABASE_NAME = "mobilewgu.db";
    private static final int SCHEMA_VERSION = 1;
    private static DatabaseHelper singleton = null;

    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, SCHEMA_VERSION);
    }

    synchronized static DatabaseHelper getInstance(Context context) {
        if (singleton == null) {
            singleton = new DatabaseHelper(context.getApplicationContext());
        }
        return singleton;
    }

    public void loadTerms() {
        new LoadTermsThread().start();
    }

    public void addTerm(String termTitle, String startDate, String endDate) {
        new InsertTermThread(termTitle, startDate, endDate).start();
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        createTermsTable(sqLiteDatabase);
        createMentorTable(sqLiteDatabase);
        createCourseTable(sqLiteDatabase);
        createAssessmentsTable(sqLiteDatabase);
        createNotesTable(sqLiteDatabase);
        createImagesTable(sqLiteDatabase);
    }

    private void createTermsTable(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE `terms` (\n" +
                "\t`_id`\tINTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                "\t`title`\tTEXT NOT NULL,\n" +
                "\t`startDate`\tTEXT NOT NULL,\n" +
                "\t`endDate`\tTEXT NOT NULL\n" +
                ")");
    }

    private void createMentorTable(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE \"courseMentor\" (\n" +
                "\t`_id`\tINTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                "\t`name`\tTEXT NOT NULL,\n" +
                "\t`phone`\tTEXT NOT NULL,\n" +
                "\t`email`\tTEXT NOT NULL,\n" +
                "\t`courseId`\tINTEGER NOT NULL,\n" +
                "\tFOREIGN KEY(`courseId`) REFERENCES `coursed`(`_id`) ON DELETE CASCADE\n" +
                ")");
    }

    private void createCourseTable(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE \"courses\" (\n" +
                "\t`_id`\tINTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                "\t`title`\tTEXT NOT NULL,\n" +
                "\t`startDate`\tTEXT,\n" +
                "\t`endDate`\tTEXT,\n" +
                "\t`status`\tTEXT NOT NULL,\n" +
                "\t`term`\tINTEGER NOT NULL,\n" +
                "\tFOREIGN KEY(`term`) REFERENCES `terms`(`_id`) ON DELETE RESTRICT\n" +
                ")");
    }

    private void createAssessmentsTable(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE \"assessments\" (\n" +
                "\t`_id`\tINTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                "\t`title`\tTEXT NOT NULL,\n" +
                "\t`dueDate`\tTEXT NOT NULL,\n" +
                "\t`type`\tTEXT NOT NULL,\n" +
                "\t`courseId`\tINTEGER NOT NULL,\n" +
                "\tFOREIGN KEY(`courseId`) REFERENCES `courses`(`_id`) ON DELETE CASCADE\n" +
                ")");
    }

    private void createNotesTable(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE \"notes\" (\n" +
                "\t`_id`\tINTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                "\t`noteTitle`\tTEXT NOT NULL,\n" +
                "\t`noteText`\tTEXT,\n" +
                "\t`courseId`\tINTEGER NOT NULL,\n" +
                "\tFOREIGN KEY(`courseId`) REFERENCES `coursed`(`id`) ON DELETE CASCADE\n" +
                ")");
    }

    private void createImagesTable(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE \"images\" (\n" +
                "\t`_id`\tINTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                "\t`date`\tTEXT NOT NULL,\n" +
                "\t`image`\tBLOB NOT NULL,\n" +
                "\t`noteId`\tINTEGER NOT NULL,\n" +
                "\tFOREIGN KEY(`noteId`) REFERENCES `notes`(`_id`) ON DELETE CASCADE\n" +
                ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
//        should never get here
        throw new RuntimeException("How did we get here?");
    }

    public void loadIndividualCourse(long courseId) {
        new LoadCourseThread(courseId).start();
    }

    public void loadTermCourses(long termId) {
        new LoadTermCoursesThread(termId).start();
    }

    public void addCourseToTerm(long term, String title, String startDate, String endDate, String status) {
        new AddCourseToTermThread(term, title, startDate, endDate, status).start();
    }

    public void deleteTerm(long termId) {
        new TermDeleteThread(termId).start();
    }

    public void deleteCourse(long courseId) {
        new CourseDeleteThread(courseId).start();
    }

    public void loadCourseMentors(long courseId) {
        new LoadCourseMentorsThread(courseId).start();
    }

    public void addCourseMentor(String name, String phone, String email, long courseId) {
        new AddCourseMentorsThread(courseId, name, email, phone).start();
    }

    public void loadSingleMentor(long mentorId) {
        new LoadSingleMentorThread(mentorId).start();
    }

    public void deleteSingleMentor(long mentorId) {
        new DeleteCourseMentorThread(mentorId).start();
    }

    public void updateSingleMentor(long mentorId, String name, String phone, String email) {
        new UpdateSingleMentorThread(mentorId, name, phone, email).start();
    }

    public void updateSingleCourse(String title, String startDate, String endDate, String status, long courseId) {
        new UpdateSingleCourseThread(title, startDate, endDate, status, courseId).start();
    }

    public void loadAssessments(long courseId) {
        new LoadAssessmentsThread(courseId).start();
    }

    public void loadSingleAssessment(long assessmentId) {
        new LoadSingleAssessmentThread(assessmentId).start();
    }

    public void insertAssessment(long courseId, String title, String dueDate, String type) {
        new InsertAssessmentThread(courseId, title, dueDate, type).start();
    }

    public void updateAssessment(String title, String dueDate, long assessmentId, String type) {
        new UpdateAssessmentThread(title, dueDate, assessmentId, type).start();
    }

    public void deleteAssessment(long assessmentId) {
        new DeleteAssessmentThread(assessmentId).start();
    }

    public void loadNotes(long courseId) {
        new LoadNotesThread(courseId).start();
    }

    public void loadSingleNote(long noteId) {
        new LoadSingleNoteThread(noteId).start();
    }

    public void saveNewNote(long courseId, String noteTitle, String noteBody) {
        new SaveNewNoteThread(courseId, noteTitle, noteBody).start();
    }

    public void updateNote(long noteId, String noteTitle, String noteBody) {
        new UpdateNoteThread(noteId, noteTitle, noteBody).start();
    }

    public void deleteNote(long noteId) {
        new DeleteNoteThread(noteId).start();
    }

    public void loadImageList(long noteId) {
        new LoadImageList(noteId).start();
    }

    public void saveImage(String imageName, byte[] image, long noteId) {
        new SaveImageThread(noteId, imageName, image).start();
    }

    public void loadSingleImage(long imageId) {
        new LoadSingleImageThread(imageId).start();
    }

    public void deleteImage(long imageId) {
        new DeleteImageThread(imageId).start();
    }

    public void loadTermData(long termId) {
        new LoadTermDataThread(termId).start();
    }

    private class InsertTermThread extends Thread {
        private String termTitle;
        private String termStartDate;
        private String termEndDate;

        InsertTermThread(String termTitle, String termStartDate, String termEndDate) {
            this.termTitle = termTitle;
            this.termStartDate = termStartDate;
            this.termEndDate = termEndDate;
        }

        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            String[] args = {termTitle, termStartDate, termEndDate};
            getWritableDatabase().execSQL("INSERT INTO terms (title, startDate, endDate) VALUES (?, ?, ?)", args);
        }
    }

    private class LoadTermsThread extends Thread {

        TermLoadedEvent termLoadedEvent;

        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            Cursor c = getReadableDatabase().rawQuery("SELECT * FROM terms;", null);

            termLoadedEvent = new TermLoadedEvent(c);
            EventBus.getDefault().post(termLoadedEvent);
        }
    }

    private class LoadTermCoursesThread extends Thread {

        private long termId;
        private CoursesLoadedEvent coursesLoadedEvent;

        LoadTermCoursesThread(long termId) {
            super();
            this.termId = termId;
        }

        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            String[] args = {String.valueOf(termId)};
            Cursor c = getReadableDatabase().rawQuery("Select * FROM courses WHERE term = ?", args);
            coursesLoadedEvent = new CoursesLoadedEvent(c);
            EventBus.getDefault().post(coursesLoadedEvent);
        }
    }

    private class LoadCourseThread extends Thread {

        private long courseId;
        private SingleCourseLoadedEvent singleCourseLoadedEvent;

        LoadCourseThread(long courseId) {
            super();
            this.courseId = courseId;
        }

        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            String[] args = {String.valueOf(courseId)};
            Cursor c = getReadableDatabase().rawQuery("Select * FROM courses WHERE _id = ?", args);
            singleCourseLoadedEvent = new SingleCourseLoadedEvent(c);
            EventBus.getDefault().post(singleCourseLoadedEvent);
        }
    }

    private class AddCourseToTermThread extends Thread {
        private long term;
        private String title;
        private String startDate;
        private String endDate;
        private String status;

        public AddCourseToTermThread(long term, String title, String startDate, String endDate, String status) {
            this.term = term;
            this.title = title;
            this.startDate = startDate;
            this.endDate = endDate;
            this.status = status;
        }

        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            String[] args = {String.valueOf(title), String.valueOf(startDate), String.valueOf(endDate), String.valueOf(status), String.valueOf(term)};
            getWritableDatabase().execSQL("INSERT INTO courses (title, startDate, endDate, status, term) VALUES (?, ?, ?, ?, ?)", args);
        }
    }

    private class TermDeleteThread extends Thread {
        private long termId;

        TermDeleteThread(long termId) {
            super();
            this.termId = termId;
        }

        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            String[] args = {String.valueOf(termId)};
            getWritableDatabase().execSQL("DELETE FROM terms WHERE _id = ?", args);
        }
    }

    private class CourseDeleteThread extends Thread {
        private long courseId;

        CourseDeleteThread(long courseId) {
            super();
            this.courseId = courseId;
        }

        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            String[] args = {String.valueOf(courseId)};
            getWritableDatabase().execSQL("DELETE FROM courses WHERE _id = ?", args);
        }
    }

    private class LoadCourseMentorsThread extends Thread {
        private long courseId;

        LoadCourseMentorsThread(long courseId) {
            this.courseId = courseId;
        }

        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            String[] args = {String.valueOf(courseId)};
            Cursor cursor = getReadableDatabase().rawQuery("SELECT * FROM courseMentor WHERE courseId = ?", args);
            CourseMentorsLoadedEvent courseMentorsLoadedEvent = new CourseMentorsLoadedEvent(cursor);
            EventBus.getDefault().post(courseMentorsLoadedEvent);
        }
    }

    private class AddCourseMentorsThread extends Thread {
        private long courseId;
        private String name;
        private String email;
        private String phone;

        AddCourseMentorsThread(long courseId, String name, String email, String phone) {
            this.courseId = courseId;
            this.email = email;
            this.name = name;
            this.phone = phone;
        }

        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            String[] args = {name, phone, email, String.valueOf(courseId)};
            getWritableDatabase().execSQL("INSERT INTO courseMentor (name, phone, email, courseId) VALUES (?, ?, ?, ?)", args);


        }
    }

    private class LoadSingleMentorThread extends Thread {
        private long mentorId;

        LoadSingleMentorThread(long mentorId) {
            this.mentorId = mentorId;
        }

        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            String[] args = {String.valueOf(mentorId)};
            Cursor cursor = getReadableDatabase().rawQuery("SELECT * FROM courseMentor WHERE _id = ?", args);
            SingleMentorLoadedEvent singleMentorLoadedEvent = new SingleMentorLoadedEvent(cursor);
            EventBus.getDefault().post(singleMentorLoadedEvent);
        }
    }

    private class DeleteCourseMentorThread extends Thread {
        private long mentorId;

        DeleteCourseMentorThread(long mentorId) {
            this.mentorId = mentorId;
        }

        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            String[] args = {String.valueOf(mentorId)};
            getWritableDatabase().execSQL("DELETE FROM courseMentor WHERE _id = ?", args);
        }
    }

    private class UpdateSingleMentorThread extends Thread {
        private long mentorId;
        private String name;
        private String phone;
        private String email;

        UpdateSingleMentorThread(long mentorId, String name, String phone, String email) {
            this.mentorId = mentorId;
            this.name = name;
            this.phone = phone;
            this.email = email;
        }

        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            String[] args = {name, phone, email, String.valueOf(mentorId)};
            getWritableDatabase().execSQL("UPDATE courseMentor SET name = ?, phone = ?, email = ? WHERE _id = ?", args);
        }
    }

    private class UpdateSingleCourseThread extends Thread {
        private String title;
        private String startDate;
        private String endDate;
        private String status;
        private long courseId;

        public UpdateSingleCourseThread(String title, String startDate, String endDate, String status, long courseId) {
            this.title = title;
            this.startDate = startDate;
            this.endDate = endDate;
            this.status = status;
            this.courseId = courseId;
        }

        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            String[] args = {title, startDate, endDate, status, String.valueOf(courseId)};
            getWritableDatabase().execSQL("UPDATE courses SET title = ?, startDate = ?, endDate = ?, status = ? WHERE _id = ?", args);
        }
    }

    private class LoadAssessmentsThread extends Thread {
        private long courseId;

        LoadAssessmentsThread(long courseId) {
            this.courseId = courseId;
        }

        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            String[] args = {String.valueOf(courseId)};
            Cursor cursor = getReadableDatabase().rawQuery("SELECT * FROM assessments WHERE courseId = ?", args);
            EventBus.getDefault().post(new AssessmentsLoadedEvent(cursor));
        }
    }

    private class LoadSingleAssessmentThread extends Thread {
        private long assessmentId;

        LoadSingleAssessmentThread(long assessmentId) {
            this.assessmentId = assessmentId;
        }

        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            String[] args = {String.valueOf(assessmentId)};
            Cursor cursor = getReadableDatabase().rawQuery("SELECT * FROM assessments WHERE _id = ?", args);
            EventBus.getDefault().post(new SingleAssessmentLoadedEvent(cursor));
        }
    }

    private class InsertAssessmentThread extends Thread {
        private long courseId;
        private String title;
        private String dueDate;
        private String type;

        InsertAssessmentThread(long courseId, String title, String dueDate, String type) {
            this.courseId = courseId;
            this.title = title;
            this.dueDate = dueDate;
            this.type = type;
        }

        @Override
        public void run() {
//            Here is the arraylist, not sure why it was a requirement, didn't end up needing it.
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);

            String[] args = {title, dueDate, type, String.valueOf(courseId)};

            getWritableDatabase().execSQL("INSERT INTO assessments (title, dueDate, type, courseId) VALUES (?, ?, ?, ?)", args);
        }
    }

    private class UpdateAssessmentThread extends Thread {
        private String title;
        private String dueDate;
        private long assessmentId;
        private String type;

        UpdateAssessmentThread(String title, String dueDate, long assessmentId, String type) {
            this.title = title;
            this.dueDate = dueDate;
            this.assessmentId = assessmentId;
            this.type = type;
        }

        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            String[] args = {title, dueDate, type, String.valueOf(assessmentId)};
            getWritableDatabase().execSQL("UPDATE assessments SET title = ?, dueDate = ?, type = ? WHERE _id = ?", args);
        }
    }

    private class DeleteAssessmentThread extends Thread {
        private long assessmentId;

        DeleteAssessmentThread(long assessmentId) {
            this.assessmentId = assessmentId;
        }

        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            String[] args = {String.valueOf(assessmentId)};
            getWritableDatabase().execSQL("DELETE FROM assessments WHERE _id = ?", args);
        }
    }

    private class LoadNotesThread extends Thread {
        private long courseId;

        LoadNotesThread(long courseId) {
            this.courseId = courseId;
        }

        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            String[] args = {String.valueOf(courseId)};
            Cursor cursor = getReadableDatabase().rawQuery("SELECT * FROM notes WHERE courseId = ?", args);
            EventBus.getDefault().post(new NotesLoadedEvent(cursor));
        }
    }

    private class LoadSingleNoteThread extends Thread {
        private long noteId;

        LoadSingleNoteThread(long noteId) {
            this.noteId = noteId;
        }

        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            String[] args = {String.valueOf(noteId)};
            Cursor cursor = getReadableDatabase().rawQuery("SELECT * FROM notes WHERE _id = ?", args);
            EventBus.getDefault().post(new SingleNoteLoadedEvent(cursor));
        }
    }

    private class SaveNewNoteThread extends Thread {
        private long courseId;
        private String noteTitle;
        private String noteBody;

        SaveNewNoteThread(long courseId, String noteTitle, String noteBody) {
            this.courseId = courseId;
            this.noteTitle = noteTitle;
            this.noteBody = noteBody;
        }

        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            String[] args = {noteTitle, noteBody, String.valueOf(courseId)};
            getWritableDatabase().execSQL("INSERT INTO notes (noteTitle, noteText, courseId) VALUES (?, ?, ?)", args);
        }
    }

    private class UpdateNoteThread extends Thread {
        private long noteId;
        private String noteTitle;
        private String noteBody;

        UpdateNoteThread(long noteId, String noteTitle, String noteBody) {
            this.noteId = noteId;
            this.noteTitle = noteTitle;
            this.noteBody = noteBody;
        }

        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            String[] args = {noteTitle, noteBody, String.valueOf(noteId)};
            getWritableDatabase().execSQL("UPDATE notes SET noteTitle = ?, noteText = ? WHERE _id = ?", args);
        }
    }

    private class DeleteNoteThread extends Thread {
        private long noteId;

        DeleteNoteThread(long noteId) {
            this.noteId = noteId;
        }

        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            String[] args = {String.valueOf(noteId)};
            getWritableDatabase().execSQL("DELETE FROM notes WHERE _id = ?", args);
        }
    }

    private class LoadImageList extends Thread {
        private long noteId;

        LoadImageList(long noteId) {
            this.noteId = noteId;
        }

        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            String[] args = {String.valueOf(noteId)};
            Cursor cursor = getWritableDatabase().rawQuery("SELECT * FROM images WHERE noteId = ?", args);
            EventBus.getDefault().post(new ImageListEvent(cursor));
        }
    }

    private class SaveImageThread extends Thread {
        private long noteId;
        private String imageName;
        private byte[] image;

        SaveImageThread(long noteId, String imageName, byte[] image) {
            this.noteId = noteId;
            this.imageName = imageName;
            this.image = image;
        }

        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);

            ContentValues values = new ContentValues();
            values.put("date", imageName);
            values.put("image", image);
            values.put("noteId", noteId);
            getWritableDatabase().insert("images", null, values);
        }
    }

    private class LoadSingleImageThread extends Thread {
        private long imageId;

        LoadSingleImageThread(long imageId) {
            this.imageId = imageId;
        }

        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            String[] args = {String.valueOf(imageId)};
            Cursor cursor = getReadableDatabase().rawQuery("SELECT * FROM images WHERE _id = ?", args);
            EventBus.getDefault().post(new SingleImageLoadedEvent(cursor));
        }
    }

    private class DeleteImageThread extends Thread {
        private long imageId;

        DeleteImageThread(long imageId) {
            this.imageId = imageId;
        }

        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            String[] args = {String.valueOf(imageId)};
            getWritableDatabase().execSQL("DELETE FROM images WHERE _id = ?", args);
        }
    }

    private class LoadTermDataThread extends Thread {
        private long termId;

        LoadTermDataThread(long termId) {
            this.termId = termId;
        }

        @Override
        public void run() {
            Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
            String[] args = {String.valueOf(termId)};
            Cursor cursor = getReadableDatabase().rawQuery("SELECT title, endDate, startDate FROM terms WHERE _id = ?", args);
            EventBus.getDefault().post(new TermDateLoadedEvent(cursor));
        }
    }


}
