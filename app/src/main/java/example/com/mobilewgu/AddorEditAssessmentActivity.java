package example.com.mobilewgu;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import de.greenrobot.event.EventBus;
import example.com.mobilewgu.events.SingleAssessmentLoadedEvent;

/*This is where assessments are added and updated.*/
public class AddorEditAssessmentActivity extends AppCompatActivity {
    public static final String COURSE_ID_EXTRA = "courseidextra";
    public static final String ASSESSMENT_ID_EXTRA = "assessmentidextra";
    private static final int DUE_DATE_DIALOG = 1;
    private static final String END_DATE_FIELD = "enddatefield";
    private static final String END_DATE_CAL = "enddatecal";
    //    Array List is here.
    private static final ArrayList<String> ASSESSMENT_TYPE = new ArrayList<>();
    private Button dueDateButton;
    private EditText title;
    private Button submitButton;
    private TextView assessmentLabel;
    private Spinner assessmentType;
    private long courseId;
    private long assessmentId;
    private Calendar dueDate = Calendar.getInstance();
    private DatabaseHelper databaseHelper;
    private DatePickerDialog.OnDateSetListener dueDatePickerListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
            dueDate.set(i, i1, i2);
            dueDateButton.setText(formatDate(dueDate));
        }
    };

    static {
        ASSESSMENT_TYPE.add("Objective");
        ASSESSMENT_TYPE.add("Performance");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        dueDateButton.setText(savedInstanceState.getString(END_DATE_FIELD));

        dueDate = (Calendar) savedInstanceState.getSerializable(END_DATE_CAL);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        if (id == 1) {
            return new DatePickerDialog(this, dueDatePickerListener, dueDate.get(Calendar.YEAR), dueDate.get(Calendar.MONTH), dueDate.get(Calendar.DAY_OF_MONTH));
        }
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        databaseHelper = DatabaseHelper.getInstance(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_or_edit_assessment);
        dueDateButton = (Button) findViewById(R.id.dueDateDialog);
        title = (EditText) findViewById(R.id.assessmentTitle);
        submitButton = (Button) findViewById(R.id.submitAssessment);
        assessmentLabel = (TextView) findViewById(R.id.addAssessmentLabel);
        Intent intent = getIntent();
        courseId = intent.getLongExtra(COURSE_ID_EXTRA, -1);
        assessmentId = intent.getLongExtra(ASSESSMENT_ID_EXTRA, -1);
        assessmentType = (Spinner) findViewById(R.id.assessmentType);
        assessmentType.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, ASSESSMENT_TYPE));
        if (assessmentId != -1)
            assessmentLabel.setText("Edit Assessment");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(END_DATE_FIELD, dueDateButton.getText().toString());

        outState.putSerializable(END_DATE_CAL, dueDate);
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        if (assessmentId != -1) {
            getAssessmentData();
        }
    }

    private void getAssessmentData() {
        databaseHelper.loadSingleAssessment(assessmentId);
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(SingleAssessmentLoadedEvent event) {
        loadAssessmentData(event.getCursor());
    }

    private void loadAssessmentData(Cursor cursor) {
        cursor.moveToFirst();
        title.setText(cursor.getString(1));
        DateFormat simpleDateFormat = SimpleDateFormat.getDateInstance();

        String date = cursor.getString(2);
        try {
            dueDate.setTime(simpleDateFormat.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        dueDateButton.setText(formatDate(dueDate));
        switch (cursor.getString(3)) {
            case "Objective":
                assessmentType.setSelection(0);
                break;
            case "Performance":
                assessmentType.setSelection(1);
                break;
        }

    }

    private String formatDate(Calendar calendar) {
        DateFormat dateFormat = DateFormat.getDateInstance();
        return dateFormat.format(calendar.getTime());
    }

    public void submitAssessment(View view) {
//        if assessment id is -1, we are adding a new assessment.
        if (assessmentId == -1) {
            if (TextUtils.isEmpty(title.getText())) {
                Toast.makeText(AddorEditAssessmentActivity.this, "Title Empty!", Toast.LENGTH_SHORT).show();
                return;
            }
            databaseHelper.insertAssessment(courseId, title.getText().toString(), formatDate(dueDate), assessmentType.getSelectedItem().toString());
            Toast.makeText(AddorEditAssessmentActivity.this, "Assessment Added!", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        if (TextUtils.isEmpty(title.getText())) {
            Toast.makeText(AddorEditAssessmentActivity.this, "Title Empty!", Toast.LENGTH_SHORT).show();
            return;
        }
        databaseHelper.updateAssessment(title.getText().toString(), formatDate(dueDate), assessmentId, assessmentType.getSelectedItem().toString());
        Toast.makeText(AddorEditAssessmentActivity.this, "Assessment Updated!", Toast.LENGTH_SHORT).show();
        finish();
    }

    public void showDueDateDialog(View view) {
        showDialog(DUE_DATE_DIALOG);
    }
}
