package example.com.mobilewgu;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ShareActionProvider;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import de.greenrobot.event.EventBus;
import example.com.mobilewgu.events.SingleNoteLoadedEvent;

public class NotesDetailActivity extends AppCompatActivity {
    public static final String NOTE_ID_EXTRA = "noteidextra";
    public static final String COURSE_ID_EXTRA = "courseidextra";
    private long courseId;
    private long noteId;
    private EditText noteTitle;
    private EditText noteBody;
    private DatabaseHelper databaseHelper;
    private ShareActionProvider share = null;
    private Intent shareIntent = new Intent(Intent.ACTION_SEND).setType("text/plain");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes_detail);
        databaseHelper = DatabaseHelper.getInstance(this);
        noteTitle = (EditText) findViewById(R.id.noteTitle);
        noteBody = (EditText) findViewById(R.id.noteBody);
        Intent intent = getIntent();
        noteId = intent.getLongExtra(NOTE_ID_EXTRA, -1);
        courseId = intent.getLongExtra(COURSE_ID_EXTRA, -1);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.note_detail_menu, menu);
        MenuItem item = menu.findItem(R.id.shareNote);
        share = new ShareActionProvider(this);
        MenuItemCompat.setActionProvider(item, share);
        share.setShareIntent(shareIntent);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        Intent intent = getIntent();
        if (intent.getLongExtra(NOTE_ID_EXTRA, -1) == -1) {
            menu.findItem(R.id.deleteNote).setVisible(false);
            menu.findItem(R.id.shareNote).setVisible(false);
            menu.findItem(R.id.viewNotePhotos).setVisible(false);
        }
        super.onPrepareOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.saveNote:
                if (TextUtils.isEmpty(noteTitle.getText())) {
                    Toast.makeText(NotesDetailActivity.this, "Title can't be empty!", Toast.LENGTH_SHORT).show();
                    return true;
                }
                if (noteId == -1) {
                    databaseHelper.saveNewNote(courseId, noteTitle.getText().toString(), noteBody.getText().toString());
                    Toast.makeText(NotesDetailActivity.this, "New note saved!", Toast.LENGTH_SHORT).show();
                    finish();
                    return true;
                } else {
                    databaseHelper.updateNote(noteId, noteTitle.getText().toString(), noteBody.getText().toString());
                    Toast.makeText(NotesDetailActivity.this, "Note Changes Saved!", Toast.LENGTH_SHORT).show();
                    finish();
                    return true;
                }
            case R.id.deleteNote:
                databaseHelper.deleteNote(noteId);
                Toast.makeText(NotesDetailActivity.this, "Note Deleted!", Toast.LENGTH_SHORT).show();
                finish();
                return true;
            case R.id.shareNote:
                databaseHelper.updateNote(noteId, noteTitle.getText().toString(), noteBody.getText().toString());
                shareIntent.putExtra(Intent.EXTRA_TEXT, noteBody.getText().toString());
                return true;
            case R.id.viewNotePhotos:
                databaseHelper.updateNote(noteId, noteTitle.getText().toString(), noteBody.getText().toString());
                Intent intent = new Intent(this, ImageListActivity.class);
                intent.putExtra(ImageListActivity.NOTE_ID_EXTRA, noteId);
                startActivity(intent);
                return true;
        }
        return false;
    }

    @Override
    protected void onPause() {

        EventBus.getDefault().unregister(this);
        super.onPause();

    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        if (noteId != -1) {
            databaseHelper.loadSingleNote(noteId);
        }
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(SingleNoteLoadedEvent event) {
        loadNote(event.getCursor());
    }

    private void loadNote(Cursor cursor) {
        cursor.moveToFirst();
        noteTitle.setText(cursor.getString(1));
        noteBody.setText(cursor.getString(2));
    }


}
