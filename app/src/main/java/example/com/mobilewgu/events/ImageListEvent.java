package example.com.mobilewgu.events;

import android.database.Cursor;

/**
 * Created by beams on 5/15/2016.
 */
public class ImageListEvent {
    private Cursor cursor;

    public ImageListEvent(Cursor cursor) {

        this.cursor = cursor;
    }

    public Cursor getCursor() {
        return cursor;
    }
}
