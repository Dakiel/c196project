package example.com.mobilewgu.events;

import android.database.Cursor;

/**
 * Created by beams on 5/11/2016.
 */
public class TermLoadedEvent {
    private Cursor c;

    public TermLoadedEvent(Cursor cursor) {
        c = cursor;
    }

    public Cursor getCursor() {
        return c;
    }
}
