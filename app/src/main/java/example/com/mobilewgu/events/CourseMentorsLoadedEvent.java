package example.com.mobilewgu.events;

import android.database.Cursor;

/**
 * Created by beams on 5/12/2016.
 */
public class CourseMentorsLoadedEvent {
    private Cursor cursor;

    public CourseMentorsLoadedEvent(Cursor cursor) {

        this.cursor = cursor;
    }

    public Cursor getCursor() {
        return cursor;
    }
}
