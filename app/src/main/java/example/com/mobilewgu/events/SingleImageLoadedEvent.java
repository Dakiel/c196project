package example.com.mobilewgu.events;

import android.database.Cursor;

/**
 * Created by beams on 5/16/2016.
 */
public class SingleImageLoadedEvent {
    private Cursor cursor;

    public SingleImageLoadedEvent(Cursor cursor) {

        this.cursor = cursor;

    }

    public Cursor getCursor() {
        return cursor;
    }
}
