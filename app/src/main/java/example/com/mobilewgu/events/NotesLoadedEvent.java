package example.com.mobilewgu.events;

import android.database.Cursor;

/**
 * Created by beams on 5/14/2016.
 */
public class NotesLoadedEvent {
    private Cursor cursor;

    public NotesLoadedEvent(Cursor cursor) {

        this.cursor = cursor;
    }

    public Cursor getCursor() {
        return cursor;
    }
}
