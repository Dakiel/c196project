package example.com.mobilewgu.events;

import android.database.Cursor;

/**
 * Created by beams on 5/17/2016.
 */
public class TermDateLoadedEvent {
    private Cursor cursor;

    public TermDateLoadedEvent(Cursor cursor) {

        this.cursor = cursor;
    }

    public Cursor getCursor() {
        return cursor;
    }
}
