package example.com.mobilewgu.events;

import android.database.Cursor;

/**
 * Created by beams on 5/12/2016.
 */
public class SingleMentorLoadedEvent {
    private Cursor cursor;

    public SingleMentorLoadedEvent(Cursor cursor) {

        this.cursor = cursor;

    }

    public Cursor getCursor() {
        return cursor;
    }
}
