package example.com.mobilewgu.events;

import android.database.Cursor;

/**
 * Created by beams on 5/11/2016.
 */
public class CoursesLoadedEvent {
    private Cursor cursor;

    public CoursesLoadedEvent(Cursor cursor) {

        this.cursor = cursor;
    }

    public Cursor getCursor() {
        return cursor;
    }
}
