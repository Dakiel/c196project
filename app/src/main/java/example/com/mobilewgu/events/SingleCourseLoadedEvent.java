package example.com.mobilewgu.events;

import android.database.Cursor;

/**
 * Created by beams on 5/11/2016.
 */
public class SingleCourseLoadedEvent {

    private Cursor cursor;

    public SingleCourseLoadedEvent(Cursor cursor) {

        this.cursor = cursor;

    }

    public Cursor getCursor() {
        return cursor;
    }
}
