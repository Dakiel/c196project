package example.com.mobilewgu.events;

import android.database.Cursor;

/**
 * Created by beams on 5/14/2016.
 */
public class SingleAssessmentLoadedEvent {
    private Cursor cursor;

    public SingleAssessmentLoadedEvent(Cursor cursor) {

        this.cursor = cursor;
    }

    public Cursor getCursor() {
        return cursor;
    }
}
