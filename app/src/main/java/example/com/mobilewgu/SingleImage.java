package example.com.mobilewgu;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import de.greenrobot.event.EventBus;
import example.com.mobilewgu.events.SingleImageLoadedEvent;

public class SingleImage extends AppCompatActivity {


    public static final String IMAGE_ID_EXTRA = "imageidextra";
    private DatabaseHelper databaseHelper;
    private long imageId;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_image);
        imageView = (ImageView) findViewById(R.id.photoView);
        Intent intent = getIntent();
        imageId = intent.getLongExtra(IMAGE_ID_EXTRA, -1);
        databaseHelper = DatabaseHelper.getInstance(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        databaseHelper.loadSingleImage(imageId);

    }

    @SuppressWarnings("unused")
    public void onEventMainThread(SingleImageLoadedEvent event) {
        loadImageData(event.getCursor());
    }

    private void loadImageData(Cursor cursor) {
        cursor.moveToFirst();
        byte[] imageByteArray = cursor.getBlob(2);
        Bitmap bitmap = BitmapFactory.decodeByteArray(imageByteArray, 0, imageByteArray.length);
        imageView.setImageBitmap(bitmap);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.single_photo_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.deletePhoto:
                databaseHelper.deleteImage(imageId);
                finish();
                return true;
        }
        return false;
    }
}
