package example.com.mobilewgu;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import de.greenrobot.event.EventBus;
import example.com.mobilewgu.events.NotesLoadedEvent;

public class NotesListActivity extends AppCompatActivity {
    public static final String COURSE_ID_EXTRA = "courseidextra";
    private DatabaseHelper databaseHelper;
    private ListView notesListView;
    private long courseId;
    private TextView noNotes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes_list);
        databaseHelper = DatabaseHelper.getInstance(this);
        Intent intent = getIntent();
        courseId = intent.getLongExtra(COURSE_ID_EXTRA, -1);
        notesListView = (ListView) findViewById(R.id.notesListView);
        noNotes = (TextView) findViewById(R.id.noNotes);
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        databaseHelper.loadNotes(courseId);

    }

    @SuppressWarnings("unused")
    public void onEventMainThread(NotesLoadedEvent event) {
        loadNotes(event.getCursor());
    }

    private void loadNotes(Cursor cursor) {
        String[] from = {DatabaseHelper.NOTES_COLUMNS[1]};
        int[] to = {android.R.id.text1};
        CursorAdapter cursorAdapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_1, cursor, from, to, 0);
        notesListView.setAdapter(cursorAdapter);
        notesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getApplicationContext(), NotesDetailActivity.class);
                intent.putExtra(NotesDetailActivity.NOTE_ID_EXTRA, l);
                startActivity(intent);
            }
        });
        if (notesListView.getCount() > 0) {
            noNotes.setVisibility(View.GONE);
        } else {
            noNotes.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.notes_list_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.addNote:
                Intent intent = new Intent(this, NotesDetailActivity.class);
                intent.putExtra(NotesDetailActivity.COURSE_ID_EXTRA, courseId);
                startActivity(intent);
                return true;
        }
        return false;
    }
}
