package example.com.mobilewgu;

/**
 * Created by beams on 5/10/2016.
 */
public enum CourseStatus {
    PLAN_TO_TAKE("Plan to take"),
    IN_PROGRESS("In Progress"),
    COMPLETED("Completed"),
    DROPPED("Dropped");

    private final String courseStatusLabel;


    CourseStatus(String courseStatusLabel) {
        this.courseStatusLabel = courseStatusLabel;
    }

    public String getCourseStatusLabel() {
        return courseStatusLabel;
    }

    @Override
    public String toString() {
        return courseStatusLabel;
    }
}
