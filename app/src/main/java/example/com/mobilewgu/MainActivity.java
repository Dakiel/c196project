package example.com.mobilewgu;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.util.Calendar;

import de.greenrobot.event.EventBus;
import example.com.mobilewgu.events.TermLoadedEvent;

/* Here is where it all starts, terms and student name are shown here. */
public class MainActivity extends AppCompatActivity {
    private static final String STUDENT_NAME_KEY = "studentnamekey";
    private DatabaseHelper databaseHelper;
    private ListView termListView;
    private TextView noTerms;
    private EditText studentName;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        databaseHelper = DatabaseHelper.getInstance(this);
        termListView = (ListView) findViewById(android.R.id.list);
        noTerms = (TextView) findViewById(R.id.noTerms);
        studentName = (EditText) findViewById(R.id.studentName);
        sharedPreferences = this.getPreferences(Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        alertTest();


    }

    //    This is for you, the reviewer. To see that the alerts work without waiting forever. After 20 seconds
//    an alert will appear using the same mechanism as the others.
    private void alertTest() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND, 20);
        Intent intent = new Intent(this, AlarmReceiver.class);
        intent.putExtra(AlarmReceiver.ALERT_MESSAGE_EXTRA, "It works!");
        intent.putExtra(AlarmReceiver.ALERT_TITLE_EXTRA, "Title");
        PendingIntent sender = PendingIntent.getBroadcast(this, AlarmReceiver.REQUEST_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), sender);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        //noinspection SimplifiableIfStatement
        switch (item.getItemId()) {
            case R.id.addTerm:
                Intent intent = new Intent(this, AddTerm.class);
                startActivity(intent);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //        below method is called via reflection, part of the event bus library
    @SuppressWarnings("unused")
    public void onEventMainThread(TermLoadedEvent event) {

        loadTerms(event.getCursor());

    }


    private void loadTerms(Cursor termList) {
        String[] from = {DatabaseHelper.TERM_COLUMNS[1]};
        int[] to = {android.R.id.text1};
        CursorAdapter cursorAdapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_1, termList, from, to, 0);


        termListView.setAdapter(cursorAdapter);
        termListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getApplicationContext(), TermActivity.class);
                intent.putExtra(TermActivity.EXTRA_POSITION, l);
                startActivity(intent);
            }
        });
        if (termListView.getCount() > 0) {
            noTerms.setVisibility(View.GONE);
        } else {
            noTerms.setVisibility(View.VISIBLE);
        }

    }

    @Override
    protected void onPause() {
        editor.putString(STUDENT_NAME_KEY, studentName.getText().toString());
        editor.commit();
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        databaseHelper.loadTerms();
        String studentNameString = sharedPreferences.getString(STUDENT_NAME_KEY, null);

        if (studentNameString != null)
            studentName.setText(studentNameString);


    }


}
