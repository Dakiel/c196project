package example.com.mobilewgu;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import de.greenrobot.event.EventBus;
import example.com.mobilewgu.events.SingleCourseLoadedEvent;

public class AddOrEditCourse extends AppCompatActivity {

    public static final String TERM_ID_EXTRA = "termId";
    public static final String COURSE_ID_EXTRA = "courseidextra";
    private static final String START_DATE_FIELD = "startdatefield";
    private static final String END_DATE_FIELD = "enddatefield";
    private static final String START_DATE_CAL = "startdatecal";
    private static final String END_DATE_CAL = "enddatecal";
    private static final int START_DIALOG_ID = 1;
    private static final int END_DIALOG_ID = 2;
    private Spinner statusSpinner;
    private DatabaseHelper databaseHelper;
    private EditText courseTitle;
    private Button startDateButton;
    private Button endDateButton;
    private Calendar startDate = Calendar.getInstance();
    private Calendar endDate = Calendar.getInstance();
    private long courseId;
    private long termId;
    private DatePickerDialog.OnDateSetListener startDatePickerListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
            startDate.set(i, i1, i2);
            startDateButton.setText(formatDate(startDate));
        }
    };
    private DatePickerDialog.OnDateSetListener endDatePickerListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
            endDate.set(i, i1, i2);
            endDateButton.setText(formatDate(endDate));
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_course_to_term);
        termId = getIntent().getExtras().getLong(TERM_ID_EXTRA);
        courseId = getIntent().getLongExtra(COURSE_ID_EXTRA, -1);
        databaseHelper = DatabaseHelper.getInstance(this);
        courseTitle = (EditText) findViewById(R.id.courseTitle);
        startDateButton = (Button) findViewById(R.id.addCourseStartDate);
        endDateButton = (Button) findViewById(R.id.addCourseEndDate);
        statusSpinner = (Spinner) findViewById(R.id.courseStatusSpinner);
        statusSpinner.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, CourseStatus.values()));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(START_DATE_FIELD, startDateButton.getText().toString());
        outState.putString(END_DATE_FIELD, endDateButton.getText().toString());
        outState.putSerializable(START_DATE_CAL, startDate);
        outState.putSerializable(END_DATE_CAL, endDate);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        startDateButton.setText(savedInstanceState.getString(START_DATE_FIELD));
        endDateButton.setText(savedInstanceState.getString(END_DATE_FIELD));
        startDate = (Calendar) savedInstanceState.getSerializable(START_DATE_CAL);
        endDate = (Calendar) savedInstanceState.getSerializable(END_DATE_CAL);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case START_DIALOG_ID:
                return new DatePickerDialog(this, startDatePickerListener, startDate.get(Calendar.YEAR), startDate.get(Calendar.MONTH), startDate.get(Calendar.DAY_OF_MONTH));
            case END_DIALOG_ID:
                return new DatePickerDialog(this, endDatePickerListener, endDate.get(Calendar.YEAR), endDate.get(Calendar.MONTH), endDate.get(Calendar.DAY_OF_MONTH));
        }
        return null;
    }

    public void submitCourse(View view) {
        if (courseId == -1) {
            if (TextUtils.isEmpty(courseTitle.getText())) {
                Toast.makeText(AddOrEditCourse.this, "All Fields must be filled!", Toast.LENGTH_SHORT).show();
                return;
            }
            if (startDate.after(endDate)) {
                Toast.makeText(AddOrEditCourse.this, "Start date cannot be after end date!", Toast.LENGTH_SHORT).show();
                return;
            }
            databaseHelper.addCourseToTerm(termId, courseTitle.getText().toString(), formatDate(startDate), formatDate(endDate), statusSpinner.getSelectedItem().toString());
            Toast.makeText(AddOrEditCourse.this, "Course Added!", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        if (TextUtils.isEmpty(courseTitle.getText())) {
            Toast.makeText(AddOrEditCourse.this, "All Fields must be filled!", Toast.LENGTH_SHORT).show();
            return;
        }
        if (startDate.after(endDate)) {
            Toast.makeText(AddOrEditCourse.this, "Start date cannot be after end date!", Toast.LENGTH_SHORT).show();
            return;
        }
        databaseHelper.updateSingleCourse(courseTitle.getText().toString(), formatDate(startDate), formatDate(endDate), statusSpinner.getSelectedItem().toString(), courseId);
        Toast.makeText(AddOrEditCourse.this, "Course Updated!", Toast.LENGTH_SHORT).show();
        finish();
    }

    private String formatDate(Calendar calendar) {
        DateFormat dateFormat = DateFormat.getDateInstance();
        return dateFormat.format(calendar.getTime());
    }

    public void showCourseEndPickerDialog(View view) {
        showDialog(END_DIALOG_ID);
    }

    public void showCourseStartPickerDialog(View view) {
        showDialog(START_DIALOG_ID);
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        if (courseId != -1)
            getCourse();
    }

    private void getCourse() {
        databaseHelper.loadIndividualCourse(courseId);
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(SingleCourseLoadedEvent event) {
        loadCourse(event.getCursor());
    }

    private void loadCourse(Cursor cursor) {
        cursor.moveToFirst();
        courseTitle.setText(cursor.getString(1));
        DateFormat simpleDateFormat1 = SimpleDateFormat.getDateInstance();
        DateFormat simpleDateFormat2 = SimpleDateFormat.getDateInstance();
        try {

            startDate.setTime(simpleDateFormat1.parse(cursor.getString(2)));


        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {

            endDate.setTime(simpleDateFormat2.parse(cursor.getString(3)));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        startDateButton.setText(formatDate(startDate));
        endDateButton.setText(formatDate(endDate));
        switch (cursor.getString(4)) {
            case "Plan to take":
                statusSpinner.setSelection(0);
                break;
            case "In Progress":
                statusSpinner.setSelection(1);
                break;
            case "Completed":
                statusSpinner.setSelection(2);
                break;
            case "Dropped":
                statusSpinner.setSelection(3);
        }

    }
}
