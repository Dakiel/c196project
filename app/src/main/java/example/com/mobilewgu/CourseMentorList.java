package example.com.mobilewgu;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import de.greenrobot.event.EventBus;
import example.com.mobilewgu.events.CourseMentorsLoadedEvent;

public class CourseMentorList extends AppCompatActivity {
    public static final String COURSE_ID_EXTRA = "COURSE_ID_EXTRA";
    private DatabaseHelper databaseHelper;
    private ListView mentorListView;
    private long courseId;
    private TextView noMentors;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_mentor_list);
        databaseHelper = DatabaseHelper.getInstance(this);
        Intent intent = getIntent();
        courseId = intent.getLongExtra(COURSE_ID_EXTRA, -1);
        mentorListView = (ListView) findViewById(R.id.courseMentorListView);
        noMentors = (TextView) findViewById(R.id.noMentors);
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        databaseHelper.loadCourseMentors(courseId);

    }

    @SuppressWarnings("unused")
    public void onEventMainThread(CourseMentorsLoadedEvent event) {
        loadCourseMentors(event.getCursor());
    }

    private void loadCourseMentors(Cursor cursor) {
        String[] from = {DatabaseHelper.COURSE_MENTOR_COLUMNS[1], "_id"};
        int[] to = {android.R.id.text1};

        CursorAdapter cursorAdapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_1, cursor, from, to, 0);
        mentorListView.setAdapter(cursorAdapter);
        mentorListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getApplicationContext(), CourseMentorDetailView.class);
                intent.putExtra(CourseMentorDetailView.MENTOR_ID_EXTRA, l);
                startActivity(intent);
            }
        });
        if (mentorListView.getCount() > 0) {
            noMentors.setVisibility(View.GONE);
        } else {
            noMentors.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.course_mentor_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.addCourseMentor:
                Intent intent = new Intent(this, AddOrEditCourseMentor.class);
                intent.putExtra(AddOrEditCourseMentor.COURSE_ID_EXTRA, courseId);
                startActivity(intent);
                return true;
        }
        return false;
    }
}
